// ==UserScript==
// @name        Bittrex
// @namespace   bittrex.com
// @description Preencher os campos de venda do Bittrex com valores customizados
// @include     *://bittrex.*/*
// @require     https://code.jquery.com/jquery-3.2.1.min.js
// @version     0.3a
// @grant       none
// ==/UserScript==

$(function(){
    var percentual = 10;
  
  	function obterConteudoPorTextOuVal(campo) {
        var ret = !!($(campo).text()) ? $(campo).text() : $(campo).val();
      	return ret == NaN ? 0 : ret;
    } 
  	
  	function obterValorDoCampo(campo) {
      	return new Promise((res, rej) => {
            var val = parseFloat(obterConteudoPorTextOuVal(campo));
          	if (!!val) res(val); 
            var count = 0;
            var interval = setInterval(function() {
                var currentVal = parseFloat(obterConteudoPorTextOuVal(campo));

                if (currentVal != val) {
                    val = currentVal;
                    if (!!val) {
                      clearInterval(interval);
                      res(val);
                    }
                }
	              if (count++ > 50) {
  	              clearInterval(interval);
    		          rej('não foi possível obter o valor');
        		    }
            }, 500); 
        }) 
    } 
    
  	// nomes dos elementos da tela
  	const CAMPO_BTC = '#availableBaseCurrency';
    const CAMPO_VALOR_MAIS_RECENTE = '.market-stats div:nth-child(2) div div:nth-child(3) span';
    const BTN_VALOR_MAIS_RECENTE_COMPRA = '#menu_PriceBuy li:nth-child(1) a';
    const BTN_MAX_COMPRA = '#form_Buy div:first-child div span button';
    const IPT_QUANTIDADE_COMPRA = "input[name='quantity_Buy']";
    const BTN_VENDA_CONDICIONAL = "#form_Sell div:nth-child(3) div.col-md-3 ul li:last-child a";
    const IPT_TOTAL_COMPRA = "input[name='total_Buy']";
    const IPT_QUANTIDADE_VENDA = "input[name='quantity_Sell']";
    const BTN_VALOR_MAIS_RECENTE_VENDA = '#menu_PriceSell li:nth-child(1) a';
    const IPT_PRECO_VENDA = "input[name='price_Sell']";
    const IPT_ALVO_VENDA = "input[name='target_Sell']";
    const IPT_TOTAL_VENDA = "input[name='total_Sell']"
    const BTN_SUBMIT = "#form_Buy button[type='submit']";
    const BTN_UPDATE_VALS = '#gm_updateVals_buy';
  
  	var btc, preco
    function atualizarCampos() {
      percentual = parseFloat($('#gm_iptPercent').val());
      obterValorDoCampo(CAMPO_BTC)
          .then(r=>{ btc = r; return obterValorDoCampo(CAMPO_VALOR_MAIS_RECENTE) })
          .then(r=>{ preco = r; valoresObtidos(); })
    }
      
      
    function valoresObtidos() {
	      $(BTN_VALOR_MAIS_RECENTE_COMPRA)[0].click();
				$(BTN_MAX_COMPRA).click();
        obterValorDoCampo(IPT_QUANTIDADE_COMPRA).then($(BTN_MAX_COMPRA).click());
        $(IPT_TOTAL_COMPRA).val(btc  * (percentual / 100));	
        var qtd = $(IPT_QUANTIDADE_COMPRA).val() * (percentual / 100);	
        $(IPT_QUANTIDADE_COMPRA).val(qtd)
        $(IPT_QUANTIDADE_VENDA).val(qtd)
        $(BTN_VENDA_CONDICIONAL)[0].click();
	      $(BTN_VALOR_MAIS_RECENTE_VENDA)[0].click();
        var difCompra, difVenda, totalCompra, precoVenda;
        difCompra = $(IPT_TOTAL_COMPRA).val()
        difCompra = difCompra / (qtd * preco);
        difVenda = difCompra / (qtd * preco * 1.0025);
        precoVenda = preco * difCompra * difVenda;
        $(IPT_PRECO_VENDA).val(precoVenda);      
        $(IPT_ALVO_VENDA).val(precoVenda * 1.1);      
        $(IPT_TOTAL_VENDA).val(qtd* precoVenda);      
    }   	
  
    $(BTN_SUBMIT).parent().attr('class', 'col-md-12 input-grp-btn form-inline');
    $(BTN_SUBMIT).attr('class', 'btn btn-primary');
    $(BTN_SUBMIT).css('margin-left', '10em');
    $(BTN_SUBMIT).css('margin-top', '0');
    $(BTN_SUBMIT).parent().append("<input class='form-control text-right' value='" + percentual + "' id='gm_iptPercent' style='vertical-align: bottom;' type='text'><span style='display: inline;margin-top: 5px;' class='input-group-addon'>%</span>&nbsp;");
    $(BTN_SUBMIT).parent().append("<button type='button' id='gm_updateVals_buy' class='btn btn-primary' style='width:200px; margin-top:0px;'><i class='fa fa-refresh'></i>&nbsp;Update vals</button>");
     
    $(BTN_UPDATE_VALS).on('click', atualizarCampos);
    atualizarCampos();
});
